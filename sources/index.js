const _ = require('lodash');

const Dictionary = require('./types/Dictionary');
const Subscriber = require('./types/Subscriber');

/**
 * Store class holding a value and calling subscriber on value changes.
 */
class Store {
	/**
	 * Create new store
	 * @param {any=} value Initial value. Optional.
	 * @returns {Store} Created store
	 */
	constructor(value = undefined) {
		this._value = value;
		this.subscribers = new Dictionary();

		/**
		 * Stored data with setter invalidating store.
		 * @name value
		 * @memberof Store#
		 * @type {any}
		 */
		Object.defineProperty(this, 'value', {
			get: () => this._value,
			set: (v) => {
				this._value = v;
				this.invalidate();
			},
		});
	}

	/**
	 * Force store to refesh
	 * @returns {void}
	 */
	invalidate() {
		this.subscribers.each((subscriber) => subscriber.onChange(this._value));
	}

	/**
	 * Add subscriber function to the store
	 * @param {function} onChange Function to call when change occurs
	 * @returns {string} Id tu identify subscription
	 */
	subscribe(onChange) {
		return this.subscribers.push(new Subscriber(onChange, this._value));
	}

	/**
	 * Remove subscription from store.
	 * @param {string} id Identifier of subcribtion.
	 * @returns {void}
	 */
	unsubscribe(id) {
		this.subscribers.pop(id);
	}

	/**
	 * Create a scoped store to limit changes at part of value.
	 * @see lodash.get
	 * @param {string} path Path to expected scope
	 * @returns {Store} Proxy store.
	 */
	get(path) {
		const childStore = new Store();

		this.subscribe((value) => (childStore.value = _.get(value, path)));

		return childStore;
	}

	/**
	 * Create a scoped store to limit changes at part of value.
	 * @see lodash.find
	 * @param {function} seeker Function executed on each element in the value. First match returned.
	 * @returns {Store} Proxy store.
	 */
	find(seeker) {
		const childStore = new Store();

		this.subscribe((value) => (childStore.value = _.find(value, seeker)));

		return childStore;
	}
}


module.exports = Store;
