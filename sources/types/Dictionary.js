module.exports = class Dictionary {
	constructor() {
		this._dictionary = {};
	}

	push(data) {
		const id = Symbol('DICTIONARY_ID');
		this._dictionary[id] = data;
		return id;
	}

	pop(id) {
		delete this._dictionary[id];
	}

	each(apply) {
		Object.getOwnPropertySymbols(this._dictionary)
			.map((id) => this._dictionary[id])
			.forEach((data) => apply(data));
	}
};
