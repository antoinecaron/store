const _ = require('lodash');

module.exports = class Subscriber {
	constructor(onChange, value) {
		this._onChange = onChange;
		this.lastValue = undefined;
		this.onChange(value);
	}

	onChange(value) {
		if (!_.isEqual(value, this.lastValue)) {
			this.lastValue = _.cloneDeep(value);
			setImmediate(() => this._onChange(value));
		}
	}
};
