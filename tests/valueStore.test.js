const chai = require('chai');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

const expect = chai.expect;
chai.use(sinonChai);
const Bluebird = require('bluebird');

const Store = require('../sources');

describe('Value Store', () => {
	it('Construct store without value', () => {
		const store = new Store();
		expect(store).instanceof(Store);
		expect(store).property('value').equal(undefined);
	});

	it('Construct store with value', () => {
		const value = 42;
		const store = new Store(value);
		expect(store).instanceof(Store);
		expect(store).property('value').equal(value);
	});

	it('Invalidate should be called if value change', () => {
		const store = new Store();

		const invalidate = sinon.stub(store, 'invalidate');

		store.value = 42;

		expect(invalidate).callCount(1);
	});

	it('Subscriber should be called if value is previously defined', async () => {
		const value = 42;
		const store = new Store(value);
		const onChange = sinon.stub();

		store.subscribe(onChange);
		await Bluebird.delay(10);
		expect(onChange).calledWith(value);
	});

	it('Subscriber should be called if value is previously defined', async () => {
		const value = 42;
		const store = new Store();
		const onChange = sinon.stub();

		store.subscribe(onChange);
		store.value = value;

		await Bluebird.delay(10);
		expect(onChange).calledWith(value);
	});

	it('Subscriber should not be called if unsubscribed', async () => {
		const store = new Store();
		const onChangeCalled = sinon.stub();
		const onChangeNotCalled = sinon.stub();

		store.subscribe(onChangeCalled);
		store.unsubscribe(store.subscribe(onChangeNotCalled));

		store.value = 42;

		await Bluebird.delay(10);
		expect(onChangeCalled).callCount(1);
		expect(onChangeNotCalled).callCount(0);
	});

	it('Same item', async () => {
		const data = { id: 1, user: 'Tony' };
		const busStore = new Store(data);
		const onChange = sinon.stub();

		busStore.subscribe(onChange);
		await Bluebird.delay(10);
		expect(onChange).calledWith(data);
		onChange.resetHistory();

		data.user = 'Stephen';
		busStore.value = data;

		await Bluebird.delay(10);
		expect(onChange).calledWith(data);
		onChange.resetHistory();
	});

	it('Sub subscriber', async () => {
		const users1 = [ 'Paul', 'John', 'Garry' ];
		const users2 = [ 'Arthur', 'Boby', 'Alfred' ];
		const data = [ { id: 1, users: users1 }, { id: 2, users: users2 } ];
		const data2 = [ { id: 2, users: users1 }, { id: 1, users: users2 } ];
		const busStore = new Store(data);
		const onChange = sinon.stub();

		const buStore = busStore.find((bu) => bu.id === 1);
		const usersStore = buStore.get('users');
		usersStore.subscribe(onChange);
		await Bluebird.delay(10);
		expect(onChange).calledWith(users1);
		onChange.resetHistory();

		busStore.value = data2;
		await Bluebird.delay(10);
		expect(onChange).calledWith(users2);
		onChange.resetHistory();

		buStore.value = { id: 1, users: [ ...users1, ...users2 ] };
		await Bluebird.delay(10);
		expect(onChange).calledWith([ ...users1, ...users2 ]);
		onChange.resetHistory();
	});
});
